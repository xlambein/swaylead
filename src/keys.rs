use std::{fmt, str};

#[derive(Debug, Clone)]
pub enum KeyParseError {
    Escape,
    Unknown(String),
}

impl std::error::Error for KeyParseError {}

impl fmt::Display for KeyParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            KeyParseError::Escape => write!(f, "escape key cannot be bound"),
            KeyParseError::Unknown(s) => write!(f, "unknown key '{s}'"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Key {
    Char(char),
    Enter,
    Tab,
    Backspace,
    Left,
    Right,
    Up,
    Down,
    Ins,
    Del,
    Home,
    End,
    PageUp,
    PageDown,
    PauseBreak,
    F0,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
}

impl<'de> serde::Deserialize<'de> for Key {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = std::borrow::Cow::<str>::deserialize(deserializer)?;
        s.parse().map_err(serde::de::Error::custom)
    }
}

impl From<Key> for cursive::event::Event {
    fn from(value: Key) -> Self {
        use cursive::event::Event;
        use cursive::event::Key as CKey;
        match value {
            Key::Char(c) => Event::from(c),
            Key::Enter => Event::from(CKey::Enter),
            Key::Tab => Event::from(CKey::Tab),
            Key::Backspace => Event::from(CKey::Backspace),
            Key::Left => Event::from(CKey::Left),
            Key::Right => Event::from(CKey::Right),
            Key::Up => Event::from(CKey::Up),
            Key::Down => Event::from(CKey::Down),
            Key::Ins => Event::from(CKey::Ins),
            Key::Del => Event::from(CKey::Del),
            Key::Home => Event::from(CKey::Home),
            Key::End => Event::from(CKey::End),
            Key::PageUp => Event::from(CKey::PageUp),
            Key::PageDown => Event::from(CKey::PageDown),
            Key::PauseBreak => Event::from(CKey::PauseBreak),
            Key::F0 => Event::from(CKey::F0),
            Key::F1 => Event::from(CKey::F1),
            Key::F2 => Event::from(CKey::F2),
            Key::F3 => Event::from(CKey::F3),
            Key::F4 => Event::from(CKey::F4),
            Key::F5 => Event::from(CKey::F5),
            Key::F6 => Event::from(CKey::F6),
            Key::F7 => Event::from(CKey::F7),
            Key::F8 => Event::from(CKey::F8),
            Key::F9 => Event::from(CKey::F9),
            Key::F10 => Event::from(CKey::F10),
            Key::F11 => Event::from(CKey::F11),
            Key::F12 => Event::from(CKey::F12),
        }
    }
}

impl fmt::Display for Key {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Key::Char(c) => return c.fmt(f),
            Key::Enter => "enter",
            Key::Tab => "tab",
            Key::Backspace => "bspc",
            Key::Left => "left",
            Key::Right => "right",
            Key::Up => "up",
            Key::Down => "down",
            Key::Ins => "ins",
            Key::Del => "del",
            Key::Home => "home",
            Key::End => "end",
            Key::PageUp => "pgup",
            Key::PageDown => "pgdn",
            Key::PauseBreak => "pause",
            Key::F0 => "f0",
            Key::F1 => "f1",
            Key::F2 => "f2",
            Key::F3 => "f3",
            Key::F4 => "f4",
            Key::F5 => "f5",
            Key::F6 => "f6",
            Key::F7 => "f7",
            Key::F8 => "f8",
            Key::F9 => "f9",
            Key::F10 => "f10",
            Key::F11 => "f11",
            Key::F12 => "f12",
        }
        .fmt(f)
    }
}

impl str::FromStr for Key {
    type Err = KeyParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Key::*;
        Ok(match s.to_lowercase().as_str() {
            c if c.chars().count() == 1 => Char(c.chars().next().unwrap()),

            "enter" => Enter,
            "tab" => Tab,
            "backspace" | "bspc" => Backspace,
            "left" => Left,
            "right" => Right,
            "up" => Up,
            "down" => Down,
            "insert" | "ins" => Ins,
            "delete" | "del" => Del,
            "home" => Home,
            "end" => End,
            "page_up" | "pgup" => PageUp,
            "page_down" | "pgdn" => PageDown,
            "pause" => PauseBreak,
            "f0" => F0,
            "f1" => F1,
            "f2" => F2,
            "f3" => F3,
            "f4" => F4,
            "f5" => F5,
            "f6" => F6,
            "f7" => F7,
            "f8" => F8,
            "f9" => F9,
            "f10" => F10,
            "f11" => F11,
            "f12" => F12,

            "escape" | "esc" => return Err(KeyParseError::Escape),
            _ => return Err(KeyParseError::Unknown(s.to_owned())),
        })
    }
}

// fn display_event(event: &Event) -> String {
//     match event {
//         Event::Char(c) => c.to_string(),
//         Event::CtrlChar(c) => format!("C-{c}"),
//         Event::AltChar(c) => format!("A-{c}"),
//         Event::Key(k) => display_key(k),
//         Event::Shift(k) => format!("S-{}", display_key(k)),
//         Event::Alt(k) => format!("A-{}", display_key(k)),
//         Event::AltShift(k) => format!("A-S-{}", display_key(k)),
//         Event::Ctrl(k) => format!("C-{}", display_key(k)),
//         Event::CtrlShift(k) => format!("C-S-{}", display_key(k)),
//         Event::CtrlAlt(k) => format!("C-A-{}", display_key(k)),
//         _ => "???".to_owned(),
//     }
// }
