use std::cell::RefCell;
use std::collections::BTreeMap;
use std::rc::Rc;

use config::{Binding, Config, Keybinding};
use cursive::align::HAlign;
use cursive::event::{Event, Key};
use cursive::theme::ColorStyle;
use cursive::view::{Nameable, Resizable};
use cursive::views::{Dialog, Layer, LinearLayout, OnEventView, StackView, TextView, ViewRef};
use cursive::{Cursive, CursiveExt, View};

mod config;
mod keys;

const STACK_NAME: &str = "__SWAYLEAD_STACK";
const DIALOG_NAME: &str = "__SWAYLEAD_DIALOG";

fn main() {
    let Config { modes } = match Config::load() {
        Ok(config) => config,
        Err(err) => {
            eprintln!("error: {err:#}");
            std::process::exit(2);
        }
    };

    validate_config(&modes);

    let mut siv = Cursive::new();
    siv.add_global_callback(Key::Esc, |s| s.quit());

    let output = Rc::new(RefCell::new(None));

    let stack = modes
        .into_iter()
        .fold(StackView::new(), |view, (mode, keybindings)| {
            view.fullscreen_layer(
                view_for_keybindings(
                    keybindings,
                    {
                        let output = output.clone();
                        move |s, action| {
                            *output.borrow_mut() = Some(action.to_owned());
                            s.quit();
                        }
                    },
                    |s, mode| enable_stack_mode(s, mode),
                )
                .with_name(mode),
            )
        })
        .with_name(STACK_NAME);

    siv.add_fullscreen_layer(
        Dialog::around(stack)
            .title("swaylead")
            .title_position(HAlign::Left)
            .with_name(DIALOG_NAME),
    );

    enable_stack_mode(&mut siv, "default");

    siv.run();

    if let Some(output) = output.take() {
        println!("{output}");
    } else {
        std::process::exit(1);
    }
}

fn validate_config(modes: &std::collections::HashMap<String, Vec<Keybinding>>) {
    let mut errors = vec![];
    for (_, keybindings) in modes {
        for Keybinding { binding, .. } in keybindings {
            if let Binding::Mode(mode) = binding {
                if !modes.contains_key(mode) {
                    errors.push(format!("mode '{mode}' does not exist"));
                }
            }
        }
    }
    if !modes.contains_key("default") {
        errors.push("missing 'default' mode".to_owned());
    }
    if !errors.is_empty() {
        eprintln!("errors in configuration:");
        for error in errors {
            eprintln!("{error}");
        }
        std::process::exit(2);
    }
}

fn view_for_keybindings(
    mut keybindings: Vec<Keybinding>,
    on_action: impl Fn(&mut Cursive, &str) + Clone + 'static,
    on_mode: impl Fn(&mut Cursive, &str) + Clone + 'static,
) -> impl View {
    // Ensure that only the last of each keybinding is kept
    keybindings.reverse();
    keybindings.sort_by_key(|binding| binding.key);
    keybindings.dedup_by(|l, r| l.key == r.key);

    let mut bs = BTreeMap::new();
    for Keybinding {
        key,
        title,
        binding,
    } in keybindings
    {
        bs.entry((binding, title)).or_insert(vec![]).push(key);
    }

    let bindings: BTreeMap<_, _> = bs
        .into_iter()
        .map(|((binding, title), keys)| {
            let text = keys
                .iter()
                .map(|k| k.to_string())
                .collect::<Vec<_>>()
                .join(", ");
            let events = keys.into_iter().map(|k| Event::from(k)).collect::<Vec<_>>();
            (text, (events, binding, title))
        })
        .collect();

    let keys_width = bindings
        .iter()
        .map(|(key_text, _)| key_text.len())
        .max()
        .unwrap_or_default()
        + 2;

    let rows = bindings
        .iter()
        .enumerate()
        .fold(
            LinearLayout::vertical(),
            |layout, (i, (text, (_, binding, title)))| {
                let view = LinearLayout::horizontal()
                    .child(TextView::new(text).fixed_width(keys_width))
                    .child(
                        TextView::new(title.clone().unwrap_or_else(|| binding.to_string()))
                            .h_align(HAlign::Right)
                            .full_width(),
                    )
                    .full_width();
                if i % 2 == 0 {
                    layout.child(view)
                } else {
                    layout.child(Layer::with_color(view, ColorStyle::view().invert()))
                }
            },
        )
        .full_screen();

    bindings
        .iter()
        .fold(OnEventView::new(rows), |view, (_, (events, binding, _))| {
            events.into_iter().fold(view, |view, event| {
                let binding = binding.to_owned();
                let on_action = on_action.clone();
                let on_mode = on_mode.clone();
                view.on_event(event.clone(), move |s| match &binding {
                    Binding::Action(action) => on_action(s, action),
                    Binding::Mode(mode) => on_mode(s, mode),
                })
            })
        })
}

fn enable_stack_mode(s: &mut Cursive, mode: &str) {
    let mut stack: ViewRef<StackView> = s.find_name(STACK_NAME).unwrap();
    let pos = stack.find_layer_from_name(&mode).unwrap();
    stack.move_to_front(pos);

    let mut dialog: ViewRef<Dialog> = s.find_name(DIALOG_NAME).unwrap();
    dialog.set_title(mode);
}
