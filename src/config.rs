use std::fmt;
use std::{collections::HashMap, path::PathBuf};

use anyhow::{bail, Context, Result};
use directories::ProjectDirs;

use crate::keys::Key;

fn project_dir() -> Option<ProjectDirs> {
    ProjectDirs::from("", "", "swaylead")
}

fn config_path() -> Option<PathBuf> {
    Some(project_dir()?.config_dir().join("config.json"))
}

fn config_path_from_default_or_args() -> Result<PathBuf> {
    let args: Vec<_> = std::env::args().collect();

    let exec = PathBuf::from(&args[0]);
    let exec = exec.file_name().unwrap_or_else(|| exec.as_os_str());

    let print_usage = || -> ! {
        eprintln!(
            "Usage: {} [-c|--config-file CONFIG_FILE]",
            exec.to_string_lossy()
        );
        if let Some(config_path) = config_path() {
            if !config_path.exists() {
                eprintln!(
                    "Alternatively, specify the config in '{}'.",
                    config_path.display()
                );
            }
        }
        std::process::exit(1);
    };

    match (args.get(1).map(String::as_str), args.get(2)) {
        (Some("-c" | "--config-file"), Some(config_path)) => {
            let config_path = PathBuf::from(config_path);
            if config_path.exists() {
                return Ok(config_path);
            } else {
                bail!("config file does not exist: '{}'", config_path.display());
            }
        }
        (None, None) => {}
        _ => {
            eprintln!("CLI arguments parsing error.");
            print_usage();
        }
    }

    if let Some(config_path) = config_path() {
        if config_path.exists() {
            return Ok(config_path);
        }
    }

    eprintln!("Could not locate config file.");
    print_usage();
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Config {
    pub modes: HashMap<String, Vec<Keybinding>>,
}

impl Config {
    pub fn load() -> Result<Config> {
        let config_path = config_path_from_default_or_args()?;
        let config_file = std::fs::File::open(&config_path)
            .with_context(|| format!("could not open config file '{}'", config_path.display()))?;
        serde_json::from_reader(config_file).context("failed to parse config file")
    }
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct Keybinding {
    pub key: Key,
    #[serde(default)]
    pub title: Option<String>,
    #[serde(flatten)]
    pub binding: Binding,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord, serde::Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Binding {
    Action(String),
    Mode(String),
}

impl fmt::Display for Binding {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Binding::Action(action) => action.fmt(f),
            Binding::Mode(mode) => write!(f, "{mode} mode"),
        }
    }
}
