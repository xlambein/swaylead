# Swaylead

This is `swaylead`, a leader key-style tool for running commands in the
[`sway`][sway] window manager.

![Example of `swaylead`](./screenshot.png)

I made `swaylead` because I thought `sway` keybindings were not discoverable
enough for my taste, and I wanted to have an experience similar to the
[`helix`][helix] text editor.

## Installation

`swaylead` can be built with either `cargo build --release` or `nix build`.  If
using `cargo`, you will need to install `ncurses`.

### Home-Manager Module

If you're using [home-manager][home-manager] on NixOS with flakes, you can also
setup `swaylead` with the home-manager module within this flake:

```nix
{
  inputs.swaylead.url = "git+https://codeberg.org/xlambein/swaylead";
  # ...

  outputs = {nixpkgs, home-manager, swaylead, ...}: {
    # With stand-alone home-manager:
    homeConfigurations.username = home-manager.lib.homeManagerConfiguration {
      modules = [
        swaylead.homeManagerModules.default
        # ...
      ];
      # ...
    };
    
    # With home-manager as a NixOS module:
    nixosConfigurations.hostname = nixpkgs.lib.nixosSystem {
      modules = [
        home-manager.nixosModules.home-manager
        {
          home-manager.users.username.imports = [
            swaylead.homeManagerModules.default
          ];
          # ...
        }
        # ...
      ];
    };
  }
}
```

Once installed, the module is configured as follows:

```nix
{
  programs.swaylead = {
    enable = true;
    modes = {
      default = [
        {key = "t"; action = "exec foot";}
        # etc.
      ];
    };
  }
}
```

See below for a description of the config file format.
 
## Usage

### Triggering `swaylead`

I have `swaylead` setup as follows in my `sway` config file:

```
bindsym --release Super_L exec foot -a swaylead -e sh -c "swaylead | xargs -I'{}' swaymsg exec 'sleep 0.01 && swaymsg {}'"
```

Let's break this down:

1. I'm binding to release of `Super_L`, which is my my mod key.  This makes it
   act like a `vim` leader key, but only when it isn't used in another combo.
2. Pressing that key opens `foot`, my terminal, which itself runs `swaylead`.
   Since `swaylead` is a CLI application, you need to run it inside a terminal
   emulator.  Note the flag `-a swaylead`, which sets the app id for that
   window.
3. `swaylead` allows you to select a command to run, and then prints that
   command to the standard output.  So, next step is to pipe this into
   `swaymsg` using `xargs`.
4. What I _actually_ do is `swaymsg exec 'sleep 0.01 && swaymsg {}'`.  This is
   sort-of abusing `swaymsg` to delay the execution of the command produced by
   `swaylead`.  The reason for this is that some of the commands we want to run
   target `sway` windows, and we want the `swaylead` window to be out of the
   way when that happens.  I figured the easiest way was to wait a short amount
   of time.  If you have a better solution, I'm very curious to know!

### Configuration

The config file for `swaylead` is a plain JSON file:

```json
{
  "modes": {
    "default": [
      {
        "key": "trigger key",
        "action": "action to execute, e.g. 'layout tabbed'",
        "mode": "swaylead mode to switch to",
        "title": "optional title"
      }
    ]
  }
}
```

The file is searched in `~/.config/swaylead/config.json`, but it's also possible
to specify it as a CLI argument: `-c|--config-file CONFIG_FILE`.

`modes` is a lists of `swaylead` modes.  Each mode is a name with a list of
bindings.  `default` is the only required mode, and is enabled at startup.

Each binding must have a trigger `key`, along with either an `action` or a
`mode` to switch to.  It may also have a `title`, otherwise the `action` or
`mode` is used as title.

An `action` causes `swaylead` to print it out verbatim and exit, while a `mode`
enables a different mode in `swaylead`, allowing nested bindings.

An example config file can be found in
[`./example-config.json`](./example-config.json).

### Layout of the `swaylead` Window

I'd recommend making the `swaylead` window floating and small by default.  For
example, you could add the following to your `sway` config:

```
for_window [app_id="^swaylead$"] floating enable, sticky enable, resize set 20 ppt 30 ppt, border none
```

[sway]: https://swaywm.org/
[helix]: https://helix-editor.com/
[home-manager]: https://nix-community.github.io/home-manager/
