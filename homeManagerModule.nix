self: {
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.programs.swaylead;

  settingsFormat = pkgs.formats.json {};

  configFile =
    settingsFormat.generate "config.json" {modes = cfg.modes;};
in {
  meta.maintainers = [hm.maintainers.xlambein];

  options.programs.swaylead = {
    enable = mkEnableOption "swaylead";

    package = mkOption {
      type = types.package;
      default = self.packages.${pkgs.system}.default;
      defaultText = "pkgs.swaylead";
      description = ''
        The swaylead package to use.
      '';
    };

    modes = mkOption {
      type = types.submodule {
        freeformType = settingsFormat.type.nestedTypes.elemType;
      };
      default = {};
      description = ''
        Modes for `swaylead`, written to ~/.config/swaylead/config.json under `"modes"`.
        See https://codeberg.org/xlambein/swaylead#configuration for documentation.
      '';
      example = literalExpression ''
        {
          default = [
            { key = "t"; action = "exec foot"; title = "Launch foot terminal"; }
            { key = "w"; mode = "layout"; title = "Change layout"; }
          ];
          layout = [
            { "key" = "t"; "action" = "layout tabbed"; }
            { "key" = "s"; "action" = "layout toggle split"; }
            { "key" = "h"; "action" = "layout splith"; }
            { "key" = "v"; "action" = "layout splitv"; }
          ];
        }
      '';
    };
  };

  config = mkIf cfg.enable {
    xdg.configFile."swaylead/config.json".source = configFile;

    home.packages = [cfg.package];
  };
}
